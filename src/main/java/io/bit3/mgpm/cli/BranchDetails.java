package io.bit3.mgpm.cli;

import io.bit3.mgpm.worker.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.bit3.mgpm.cmd.Args;

import java.util.*;

public class BranchDetails {
    private final Logger logger = LoggerFactory.getLogger(BranchDetails.class);
    private final AnsiOutput output;
    private final Args args;

    public BranchDetails(Args args, AnsiOutput output) {
        this.args = args;
        this.output = output;
    }

    public void manageBranchDetails(Worker worker, BranchManager branchManager) {
        try {
            List<String> localBranchNames = branchManager.getLocalBranchNames();
            Map<String, List<String>> remoteBranchNames = branchManager.getRemoteBranchNames();

            if (localBranchNames.isEmpty() && (!args.isShowStatus() || remoteBranchNames.isEmpty())) {
                return;
            }

            Map<String, List<String>> addedRemoteBranchNames = branchManager.getAddedRemoteBranchNames();
            Map<String, List<String>> deletedRemoteBranchNames = branchManager.getDeletedRemoteBranchNames();
            Map<String, Upstream> branchUpstreamMap = branchManager.getBranchUpstreamMap();
            Map<String, Update> branchUpdateStatus = branchManager.getBranchUpdateStatus();
            Map<String, FromToIsh> branchUpdateIsh = branchManager.getBranchUpdateIsh();
            Map<String, Stats> branchStats = branchManager.getBranchStats();
            Map<String, List<String>> remoteBranchesUsedAsUpstream = new HashMap<>();

            int padding = localBranchNames.stream().mapToInt(String::length).max().orElse(1);
            String pattern = "%-" + padding + "s";
            boolean printDetails = !(!logger.isInfoEnabled() && (addedRemoteBranchNames.isEmpty()
                    && deletedRemoteBranchNames.isEmpty()
                    && branchStats.values().stream().allMatch(Stats::isEmpty)));

            if (!printDetails && logger.isWarnEnabled()) {
                return;
            }

            synchronized (output) {
                if (!printDetails) {
                    return;
                }

                output.deleteSpinner();

                output
                        .print(" * ")
                        .print(Color.YELLOW, worker.getRepositoryConfig().getPathName())
                        .println();

                for (String branchName : localBranchNames) {
                    Upstream upstream = branchUpstreamMap.get(branchName);
                    Update update = branchUpdateStatus.get(branchName);
                    Stats stats = branchStats.get(branchName);
                    String headSymbolicRef = branchManager.getHeadSymbolicRef();

                    if (upstream != null) {
                        remoteBranchesUsedAsUpstream.computeIfAbsent(upstream.getRemoteName(), k -> new LinkedList<>()).add(upstream.getRemoteBranch());
                    }

                    printBranchName(pattern, branchName, headSymbolicRef);
                    printBranchUpstream(upstream);
                    printBranchUpdate(branchName, update, branchUpdateIsh);
                    printBranchStats(stats);

                    output.println();
                }

                if (args.isShowStatus()) {
                    printRemoteBranches(pattern, remoteBranchNames, addedRemoteBranchNames, deletedRemoteBranchNames, remoteBranchesUsedAsUpstream);
                }

                output.println();
            }
        } catch (Exception exception) {
            logger.error(exception.getMessage(), exception);
        }
    }

    private void printBranchName(String pattern, String branchName, String headSymbolicRef) {
        output.print("   ");

        boolean isHead = branchName.equals(headSymbolicRef);
        if (isHead) {
            output.print(Color.BLUE, "> ");
        } else {
            output.print("  ");
        }

        output.print(pattern, branchName);
    }

    private void printBranchUpstream(Upstream upstream) {
        if (upstream != null) {
            output.print(Color.DARK_GRAY, " → %s", upstream.getRemoteRef());
        }
    }

    private void printBranchUpdate(String branchName, Update update, Map<String, FromToIsh> branchUpdateIsh) {
        if (update != null) {
            Color color = Color.YELLOW;
            switch (update) {
                case SKIP_NO_UPSTREAM:
                    return;

                case SKIP_UPSTREAM_DELETED:
                case SKIP_CONFLICTING:
                    color = Color.LIGHT_RED;
                    break;

                case UP_TO_DATE:
                case MERGED_FAST_FORWARD:
                case REBASED:
                    color = Color.GREEN;
                    break;
            }

            output
                    .print(" ")
                    .print(color, update.toString().toLowerCase().replace('_', ' '));

            switch (update) {
                case MERGED_FAST_FORWARD:
                case REBASED:
                    FromToIsh fromToIsh = branchUpdateIsh.get(branchName);
                    output
                            .print(" ")
                            .print(fromToIsh.getFrom().substring(0, 8))
                            .print("..")
                            .print(fromToIsh.getTo().substring(0, 8));
                    break;
                case SKIP_CONFLICTING:
                    output.print(" (conflict skipped)");
                    break;
                default:
                    break;
            }
        }
    }

    private void printBranchStats(Stats stats) {
        if (stats != null) {
            if (stats.isEmpty()) {
                output.print(Color.GREEN, "  ✔");
            }
            if (stats.getCommitsBehind() > 0) {
                output.print(Color.CYAN, "  ↓");
                output.print(Color.CYAN, stats.getCommitsBehind());
            }
            if (stats.getCommitsAhead() > 0) {
                output.print(Color.CYAN, "  ↑");
                output.print(Color.CYAN, stats.getCommitsAhead());
            }
            if (stats.getAdded() > 0) {
                output.print(Color.RED, "  +");
                output.print(Color.RED, stats.getAdded());
            }
            if (stats.getModified() > 0) {
                output.print(Color.YELLOW, "  ★");
                output.print(Color.YELLOW, stats.getModified());
            }
            if (stats.getRenamed() > 0) {
                output.print(Color.MAGENTA, "  ⇄");
                output.print(Color.MAGENTA, stats.getRenamed());
            }
            if (stats.getCopied() > 0) {
                output.print(Color.MAGENTA, "  ↷");
                output.print(Color.MAGENTA, stats.getCopied());
            }
            if (stats.getDeleted() > 0) {
                output.print(Color.MAGENTA, "  -");
                output.print(Color.MAGENTA, stats.getDeleted());
            }
            if (stats.getUnmerged() > 0) {
                output.print(Color.RED, "  ☠");
                output.print(Color.RED, stats.getUnmerged());
            }
        }
    }

    private void printRemoteBranches(String pattern, Map<String, List<String>> remoteBranchNames, Map<String, List<String>> addedRemoteBranchNames, Map<String, List<String>> deletedRemoteBranchNames, Map<String, List<String>> remoteBranchesUsedAsUpstream) {
        for (Map.Entry<String, List<String>> entry : remoteBranchNames.entrySet()) {
            String remoteName = entry.getKey();
            List<String> currentBranchNames = entry.getValue();
            List<String> addedBranchNames = addedRemoteBranchNames.get(remoteName);
            List<String> deletedBranchNames = deletedRemoteBranchNames.get(remoteName);

            Set<String> remoteBranches = new TreeSet<>(currentBranchNames);
            if (addedBranchNames != null) {
                remoteBranches.addAll(addedBranchNames);
            }
            if (deletedBranchNames != null) {
                remoteBranches.addAll(deletedBranchNames);
            }

            for (String remoteBranch : remoteBranches) {
                boolean usedAsUpstream = remoteBranchesUsedAsUpstream.containsKey(remoteName)
                        && remoteBranchesUsedAsUpstream.get(remoteName).contains(remoteBranch);

                if (usedAsUpstream) {
                    continue;
                }

                output
                        .print("   ")
                        .print(Color.DARK_GRAY, pattern, remoteName + "/" + remoteBranch);

                boolean wasAdded = addedBranchNames != null && addedBranchNames.contains(remoteBranch);
                boolean wasDeleted = deletedBranchNames != null && deletedBranchNames.contains(remoteBranch);

                if (wasAdded) {
                    output.print(Color.GREEN, " (added)");
                } else if (wasDeleted) {
                    output.print(Color.RED, " (removed)");
                }

                output.println();
            }
        }
    }
}
