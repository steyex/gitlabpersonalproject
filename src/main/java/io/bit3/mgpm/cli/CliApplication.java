package io.bit3.mgpm.cli;

import io.bit3.mgpm.cmd.Args;
import io.bit3.mgpm.cmd.FileUtils;
import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;
import io.bit3.mgpm.worker.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CliApplication {
    private final Logger logger = LoggerFactory.getLogger(CliApplication.class);
    private final Args args;
    private final Config config;
    private final AnsiOutput output;
    private final BranchDetails branchDetailsManager;

    public CliApplication(Args args, Config config) {
        this.args = args;
        this.config = config;
        this.output = AnsiOutput.getInstance();
        this.branchDetailsManager = new BranchDetails(args, output);
    }

    public void run() {
        List<File> knownDirectories = new LinkedList<>();
        ExecutorService executor = Executors.newFixedThreadPool(args.getThreads());

        for (final RepositoryConfig repositoryConfig : config.getRepositories()) {
            knownDirectories.add(repositoryConfig.getDirectory());

            Worker worker = new Worker(config, repositoryConfig, args.isDoInit(), args.isDoUpdate());
            worker.registerObserver(new LoggingWorkerObserver(output));
            worker.registerObserver(new CliWorkerObserver());
            executor.submit(worker);
        }
        executor.shutdown();

        try {
            while (!executor.awaitTermination(200, TimeUnit.MILLISECONDS)) {
                synchronized (output) {
                    output.rotateSpinner();
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        output.deleteSpinner();

        if (args.isShowStatus() && !args.isOmitSuperfluousWarnings()) {
            printSuperfluousDirectories(knownDirectories);
        }
        if (args.isDelete()) {
            deleteDirectories(config.getRepositories());
        }
    }

    private void deleteDirectories(List<RepositoryConfig> repositoryConfigs) {
        for (RepositoryConfig repositoryConfig : repositoryConfigs) {
            File directory = repositoryConfig.getDirectory();
            if (directory.exists() && directory.isDirectory()) {
                try {
                    FileUtils.deleteDirectory(directory.toPath());
                    output
                            .print("Deleted directory: ")
                            .print(Color.GREEN, directory.getPath())
                            .println();
                } catch (IOException e) {
                    logger.error("Failed to delete directory: " + directory.getPath(), e);
                    output
                            .print("Failed to delete directory: ")
                            .print(Color.RED, directory.getPath())
                            .println();
                }
            }
        }
    }

    private void printSuperfluousDirectories(List<File> knownDirectories) {
        Set<File> parentDirectories = knownDirectories
                .stream()
                .map(File::getParentFile)
                .collect(Collectors.toCollection(TreeSet::new));
        Set<File> seenFiles = new TreeSet<>();

        for (File parentDirectory : parentDirectories) {
            File[] files = parentDirectory.listFiles();
            if (null != files)
                Collections.addAll(seenFiles, files);
        }

        seenFiles.removeAll(knownDirectories);

        seenFiles = seenFiles.stream().filter(f -> !"mgpm.yml".equals(f.getName())).collect(Collectors.toSet());

        URI workingDirectory = Paths.get(".").toAbsolutePath().normalize().toUri();
        for (File file : seenFiles) {
            String relativePath = workingDirectory.relativize(file.toURI()).getPath();
            relativePath = relativePath.replaceFirst("/$", "");
            output
                    .print(" * ")
                    .print(Color.YELLOW, relativePath)
                    .print(" ")
                    .print(Color.RED, "superfluous")
                    .println();
        }
    }

    private class CliWorkerObserver extends AbstractWorkerObserver {
        @Override
        public void start(Worker worker) {
            output.addActiveWorker(worker.getRepositoryConfig().getPathName(), "...");
        }

        @Override
        public void activity(Activity activity, Worker worker) {
            output.addActiveWorker(worker.getRepositoryConfig().getPathName(), activity.getMessage());
        }

        @Override
        public void end(Worker worker) {
            output.removeActiveWorker(worker.getRepositoryConfig().getPathName());
            BranchManager branchManager = worker.getBranchManager();
            branchDetailsManager.manageBranchDetails(worker, branchManager);
        }
    }
}
