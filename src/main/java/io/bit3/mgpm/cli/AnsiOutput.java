package io.bit3.mgpm.cli;

import java.io.File;
import java.io.FileDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import jnr.posix.POSIX;
import jnr.posix.POSIXFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnsiOutput {
    private static final char ESCAPE = '\u001b';
    private static final Logger logger = LoggerFactory.getLogger(AnsiOutput.class);
    private static final boolean DECORATED;
    private static final String[] spinnerCharacters = new String[]{"|", "/", "-", "\\"};

    static {
        POSIX posix = POSIXFactory.getPOSIX();

        if ('\\' == File.separatorChar) {
            String ansicon = System.getenv("ANSICON");
            String conEmuAnsi = System.getenv("ConEmuANSI");
            DECORATED = null != ansicon && !ansicon.isEmpty() && !"false".equals(ansicon)
                    || null != conEmuAnsi && "ON".equals(conEmuAnsi);
        } else {
            DECORATED = posix.isatty(FileDescriptor.out);
        }
    }

    private final Map<String, String> activeWorkers = new LinkedHashMap<>();
    private int spinnerIndex = 0;
    private int writtenLines = 0;

    private static class Holder {
        private static final AnsiOutput INSTANCE = new AnsiOutput();
    }

    private AnsiOutput() {
    }

    public static AnsiOutput getInstance() {
        return Holder.INSTANCE;
    }

    public void println() {
        logger.info("");
    }

    public AnsiOutput print(String msg, Object... arguments) {
        logger.info(msg, arguments);
        return this;
    }

    public AnsiOutput print(Color foregroundColor, String msg, Object... arguments) {
        color(foregroundColor.foregroundCode, foregroundColor.foregroundIntensity);
        print(msg, arguments);
        reset();
        return this;
    }

    public AnsiOutput print(Color foregroundColor, Color backgroundColor,
                            String msg, Object... arguments) {
        color(foregroundColor.foregroundCode, foregroundColor.foregroundIntensity);
        color(foregroundColor.backgroundCode, backgroundColor.backgroundIntensity);
        print(msg, arguments);
        reset();
        return this;
    }

    public void print(int integer) {
        logger.info("{}", integer);
    }

    public void print(Color foregroundColor, int integer) {
        color(foregroundColor.foregroundCode, foregroundColor.foregroundIntensity);
        print(integer);
        reset();
    }

    public AnsiOutput print(Color foregroundColor, Color backgroundColor, int integer) {
        color(foregroundColor.foregroundCode, foregroundColor.foregroundIntensity);
        color(backgroundColor.backgroundCode, backgroundColor.backgroundIntensity);
        print(integer);
        reset();
        return this;
    }

    public synchronized void addActiveWorker(String label, String activity) {
        activeWorkers.put(label, activity);
    }

    public synchronized void removeActiveWorker(String label) {
        activeWorkers.remove(label);
    }

    public synchronized void rotateSpinner() {
        deleteSpinner();

        if (!DECORATED || activeWorkers.isEmpty()) {
            return;
        }

        int localSpinnerIndex = spinnerIndex;
        for (Map.Entry<String, String> entry : activeWorkers.entrySet()) {
            String label = entry.getKey();
            String activity = entry.getValue();

            logger.info(" ({} ) {}: {}", spinnerCharacters[localSpinnerIndex], label, activity);

            localSpinnerIndex = (localSpinnerIndex + 1) % spinnerCharacters.length;
            writtenLines++;
        }

        spinnerIndex = (spinnerIndex + 1) % spinnerCharacters.length;
    }

    public synchronized void deleteSpinner() {
        if (!DECORATED || 0 == writtenLines) {
            return;
        }

        // restore cursor position
        logger.info(ESCAPE + "[{}A", writtenLines); // cursor up

        // clear from cursor
        logger.info(ESCAPE + "[J"); // erase down

        writtenLines = 0;
    }

    private void color(int code, int intensity) {
        if (!DECORATED) {
            return;
        }

        logger.info(ESCAPE + "[{};{}m", intensity, code);
    }

    private void reset() {
        if (!DECORATED) {
            return;
        }

        logger.info(ESCAPE + "[0m");
    }
}
