package io.bit3.mgpm.worker;

import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class GitExecutor {
    private final Logger logger = LoggerFactory.getLogger(GitExecutor.class);
    private final Config config;
    private final RepositoryConfig repositoryConfig;

    public GitExecutor(Config config, RepositoryConfig repositoryConfig) {
        this.config = config;
        this.repositoryConfig = repositoryConfig;
    }

    public String git(String... arguments) throws GitProcessException {
        return git(repositoryConfig.getDirectory(), arguments);
    }

    public String git(List<String> arguments) throws GitProcessException {
        return git(repositoryConfig.getDirectory(), arguments);
    }

    public String git(File directory, List<String> arguments) throws GitProcessException {
        return git(arguments.toArray(new String[0]));
    }

    public String git(File directory, String... arguments) throws GitProcessException {
        List<String> command = buildGitCommand(arguments);
        logger.debug("[{}] > {}", directory, String.join(" ", command));
        try {
            Process process = executeCommand(directory, command);
            return handleProcessResult(process, command, directory);
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new GitProcessException("Git command execution interrupted", e);
        }
    }
    //pusheate
    private List<String> buildGitCommand(String... arguments) {
        List<String> command = new LinkedList<>();
        command.add(config.getGitConfig().getBinary());
        command.addAll(Arrays.asList(arguments));
        return command;
    }

    private Process executeCommand(File directory, List<String> command) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder()
                .directory(directory)
                .command(command);
        return processBuilder.start();
    }

    private String handleProcessResult(Process process, List<String> command, File directory) throws IOException, InterruptedException, GitProcessException {
        process.getOutputStream().close();
        int exitCode = process.waitFor();

        String output = new String(process.getInputStream().readAllBytes(), StandardCharsets.UTF_8).trim();
        String error = new String(process.getErrorStream().readAllBytes(), StandardCharsets.UTF_8).trim();

        if (exitCode != 0) {
            String message = buildErrorMessage(command, directory, exitCode, error.isEmpty() ? output : error);
            throw new GitProcessException(message);
        }

        return output;
    }

    private String buildErrorMessage(List<String> command, File directory, int exitCode, String error) {
        return String.format(
                "Execution of \"%s\" in \"%s\" failed with exit code %d: %s",
                String.join(" ", command),
                directory.getAbsolutePath(),
                exitCode,
                error
        );
    }

    public void stashChanges() throws GitProcessException {
        git("stash", "save", "--include-untracked", "--all", "Stash before mgpm update");
    }

    public void unstashChanges() throws GitProcessException {
        git("stash", "pop");
    }

    public boolean isWorkingDirectoryClean() throws GitProcessException {
        return StringUtils.isEmpty(git("status", "--porcelain", "--ignore-submodules"));
    }

    public void restoreHead(String headSymbolicRef) throws GitProcessException {
        if (headSymbolicRef != null) {
            git("checkout", headSymbolicRef);
        }
    }
}
