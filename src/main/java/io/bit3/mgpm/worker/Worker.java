package io.bit3.mgpm.worker;

import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

public class Worker implements Runnable {
  private final Logger logger = LoggerFactory.getLogger(Worker.class);
  private final List<WorkerObserver> observers = new LinkedList<>();
  private final List<Activity> journal = new LinkedList<>();
  private final Config config;
  private final RepositoryConfig repositoryConfig;
  private final boolean updateExisting;
  private boolean hasStashed = false;
  private final GitExecutor gitExecutor;
  private final RepositoryCloner repositoryCloner;
  private final BranchManager branchManager;

  public Worker(Config config, RepositoryConfig repositoryConfig, boolean cloneIfNotExists, boolean updateExisting) {
    this.config = config;
    this.repositoryConfig = repositoryConfig;
    this.updateExisting = updateExisting;
    this.gitExecutor = new GitExecutor(config, repositoryConfig);
    this.repositoryCloner = new RepositoryCloner(config, repositoryConfig);
    this.branchManager = new BranchManager(config, repositoryConfig, gitExecutor);
  }

  public void registerObserver(WorkerObserver observer) {
    observers.add(observer);
  }

  public Config getConfig() {
    return config;
  }

  public BranchManager getBranchManager(){
    return branchManager;
  }

  public RepositoryConfig getRepositoryConfig() {
    return repositoryConfig;
  }

  @Override
  public void run() {
    for (WorkerObserver observer : observers) {
      observer.start(this);
    }

    try {
      if (repositoryCloner.cloneOrReconfigureRepository()) {
        branchManager.determineHead();
        branchManager.determineRemoteBranches(branchManager.getOldRemoteBranchNames());
        branchManager.fetchRemotes();
        branchManager.determineRemoteBranches(branchManager.getRemoteBranchNames());
        branchManager.calculateRemoteBranchNameChanges();
        branchManager.determineLocalBranchesAndUpstreams();
        branchManager.determineStats();
        stashChanges();
        branchManager.updateBranches(updateExisting);
        restoreHead();
        unstashChanges();
      }
    } catch (Exception exception) {
      logger.error(exception.getMessage(), exception);
      journal.add(new Activity(Action.EXCEPTION_OCCURRED, exception.getMessage()));
    }

    for (WorkerObserver observer : observers) {
      observer.end(this);
    }
  }

  private void stashChanges() throws GitProcessException {
    if (!gitExecutor.isWorkingDirectoryClean()) {
      gitExecutor.stashChanges();
      hasStashed = true;
    }
  }

  private void unstashChanges() throws GitProcessException {
    if (hasStashed) {
      gitExecutor.unstashChanges();
      hasStashed = false;
    }
  }

  private void restoreHead() throws GitProcessException {
    gitExecutor.restoreHead(branchManager.getHeadSymbolicRef());
  }
}
