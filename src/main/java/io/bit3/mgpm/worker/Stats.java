package io.bit3.mgpm.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Stats {
    private int commitsBehind = 0;
    private int commitsAhead = 0;
    private int added = 0;
    private int modified = 0;
    private int renamed = 0;
    private int copied = 0;
    private int deleted = 0;
    private int unmerged = 0;
    private static final Logger logger = LoggerFactory.getLogger(Stats.class);

    public int getCommitsBehind() {
        return commitsBehind;
    }

    public int getCommitsAhead() {
        return commitsAhead;
    }

    public int getAdded() {
        return added;
    }

    public int getModified() {
        return modified;
    }

    public int getRenamed() {
        return renamed;
    }

    public int getCopied() {
        return copied;
    }

    public int getDeleted() {
        return deleted;
    }

    public int getUnmerged() {
        return unmerged;
    }

    public boolean isEmpty() {
        return 0 == commitsBehind && 0 == commitsAhead && isClean();
    }

    public boolean isClean() {
        return 0 == added && 0 == modified && 0 == renamed && 0 == copied && 0 == deleted && 0 == unmerged;
    }

    public void updateStats(char status) {
        logger.info("updateStats");
        switch (status) {
            case 'M':
                this.modified++;
                break;
            case 'A':
                this.added++;
                break;
            case 'D':
                this.deleted++;
                break;
            case 'R':
                this.renamed++;
                break;
            case 'C':
                this.copied++;
                break;
            case 'U':
                this.unmerged++;
                break;
            default:
                // Handle unknown status
                break;
        }
    }
    
    public void setCommitsBehind(int commitsBehind) {
        this.commitsBehind = commitsBehind;
    }

    public void setCommitsAhead(int commitsAhead) {
        this.commitsAhead = commitsAhead;
    }
}
