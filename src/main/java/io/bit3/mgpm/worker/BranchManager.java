package io.bit3.mgpm.worker;

import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BranchManager {
    private final Logger logger = LoggerFactory.getLogger(BranchManager.class);
    private final Config config;
    private final RepositoryConfig repositoryConfig;
    private final GitExecutor gitExecutor;

    private final Set<String> remoteNames = new HashSet<>();
    private final List<String> localBranchNames = new LinkedList<>();
    private final Map<String, List<String>> oldRemoteBranchNames = new HashMap<>();
    private final Map<String, List<String>> remoteBranchNames = new HashMap<>();
    private final Map<String, List<String>> deletedRemoteBranchNames = new HashMap<>();
    private final Map<String, List<String>> addedRemoteBranchNames = new HashMap<>();
    private final Map<String, Upstream> branchUpstreamMap = new HashMap<>();
    private final Map<String, Update> branchUpdateStatus = new HashMap<>();
    private final Map<String, FromToIsh> branchUpdateIsh = new HashMap<>();
    private final Map<String, Stats> branchStats = new HashMap<>();
    private static final String REV_PARSE = "rev-parse";
    private static final String CONFI = "config";
    private static final String LOCAL = "--local";
    private static final String GET ="--get";
    private String headSymbolicRef;
    private String headCommitRef;

    public BranchManager(Config config, RepositoryConfig repositoryConfig, GitExecutor gitExecutor) {
        this.config = config;
        this.repositoryConfig = repositoryConfig;
        this.gitExecutor = gitExecutor;
    }

    public List<String> getLocalBranchNames() {
        return localBranchNames;
    }

    public Map<String, List<String>> getOldRemoteBranchNames() {
        return oldRemoteBranchNames;
    }

    public Map<String, List<String>> getRemoteBranchNames() {
        return remoteBranchNames;
    }

    public Map<String, List<String>> getDeletedRemoteBranchNames() {
        return deletedRemoteBranchNames;
    }

    public Map<String, List<String>> getAddedRemoteBranchNames() {
        return addedRemoteBranchNames;
    }

    public Map<String, Upstream> getBranchUpstreamMap() {
        return branchUpstreamMap;
    }

    public Map<String, Update> getBranchUpdateStatus() {
        return branchUpdateStatus;
    }

    public Map<String, FromToIsh> getBranchUpdateIsh() {
        return branchUpdateIsh;
    }

    public Map<String, Stats> getBranchStats() {
        return branchStats;
    }

    public String getHeadSymbolicRef() {
        return headSymbolicRef;
    }

    public String getHeadCommitRef() {
        return headCommitRef;
    }

    public void determineHead() {
        try {
            headSymbolicRef = gitExecutor.git("symbolic-ref", "HEAD", "--short");
            headCommitRef = gitExecutor.git(REV_PARSE, headSymbolicRef);
        } catch (GitProcessException e) {
            try {
                headSymbolicRef = headCommitRef = gitExecutor.git(REV_PARSE, "HEAD");
            } catch (GitProcessException e2) {
                headSymbolicRef = headCommitRef = null;
            }
        }
    }

    public void determineRemoteBranches(Map<String, List<String>> remoteBranchNames) throws GitProcessException {
        remoteBranchNames.putAll(parseRemoteBranches(gitExecutor.git("branch", "-r")));
    }

    public void calculateRemoteBranchNameChanges() {
        for (Map.Entry<String, List<String>> entry : oldRemoteBranchNames.entrySet()) {
            String remoteName = entry.getKey();
            List<String> oldBranchNames = entry.getValue();
            List<String> newBranchNames = remoteBranchNames.get(remoteName);

            if (newBranchNames == null) {
                deletedRemoteBranchNames.put(remoteName, oldBranchNames);
            } else {
                LinkedList<String> deletedBranchNames = new LinkedList<>(oldBranchNames);
                deletedBranchNames.removeAll(newBranchNames);

                if (!deletedBranchNames.isEmpty()) {
                    deletedRemoteBranchNames.put(remoteName, deletedBranchNames);
                }
            }
        }

        for (Map.Entry<String, List<String>> entry : remoteBranchNames.entrySet()) {
            String remoteName = entry.getKey();
            List<String> newBranchNames = entry.getValue();
            List<String> oldBranchNames = oldRemoteBranchNames.get(remoteName);

            if (oldBranchNames == null) {
                addedRemoteBranchNames.put(remoteName, newBranchNames);
            } else {
                LinkedList<String> addedBranchNames = new LinkedList<>(newBranchNames);
                addedBranchNames.removeAll(oldBranchNames);

                if (!addedBranchNames.isEmpty()) {
                    addedRemoteBranchNames.put(remoteName, addedBranchNames);
                }
            }
        }
    }

    public void determineLocalBranchesAndUpstreams() throws GitProcessException {
        localBranchNames.addAll(parseLocalBranches(gitExecutor.git("branch")));

        for (String branchName : localBranchNames) {
            String remoteName = null;
            String remoteRef = null;
            String rebase = null;

            try {
                remoteName = gitExecutor.git(CONFI, LOCAL,GET, String.format("branch.%s.remote", branchName));
            } catch (GitProcessException e) {
                // exception means, there is no remote configured
            }

            try {
                remoteRef = gitExecutor.git(CONFI,LOCAL, GET, String.format("branch.%s.merge", branchName));
            } catch (GitProcessException e) {
                // exception means, there is no remote configured
            }

            try {
                rebase = gitExecutor.git(CONFI, LOCAL, GET, String.format("branch.%s.rebase", branchName)).toLowerCase();

                if (StringUtils.isBlank(rebase)) {
                    rebase = gitExecutor.git(CONFI, GET, "pull.rebase");
                }
            } catch (GitProcessException e) {
                // exception means, there is no remote configured
            }

            if (StringUtils.isEmpty(remoteName) || StringUtils.isEmpty(remoteRef)) {
                continue;
            }

            String remoteBranch = null;

            if (remoteRef.startsWith("refs/heads/")) {
                remoteBranch = remoteRef.substring(11);
                remoteRef = remoteName + "/" + remoteBranch;
            } else {
                remoteRef = remoteName + "/" + remoteRef;
            }
            remoteNames.add(remoteName);
            branchUpstreamMap.put(branchName, new Upstream(remoteName, remoteBranch, remoteRef, "true".equals(rebase)));
        }
    }

    public void fetchRemotes() throws GitProcessException {
        List<String> command = new LinkedList<>(Arrays.asList("fetch", "--prune", "--multiple"));
        command.addAll(remoteNames);
        gitExecutor.git(command);
    }

    public void determineStats() throws GitProcessException {
        for (String branchName : localBranchNames) {
            determineStatsForBranch(branchName);
        }
    }

    private void determineStatsForBranch(String branchName) throws GitProcessException {
        Upstream upstream = branchUpstreamMap.get(branchName);

        if (!isUpstreamAvailable(upstream)) {
            return;
        }

        Stats stats = new Stats();

        String localRef = gitExecutor.git(REV_PARSE, branchName);
        String remoteRef = gitExecutor.git(REV_PARSE, upstream.getRemoteRef());
        stats.setCommitsBehind(getCommitCount(localRef, remoteRef));
        stats.setCommitsAhead(getCommitCount(remoteRef, localRef));

        if (Objects.equals(headCommitRef, localRef)) {
            updateStatsFromStatus(stats);
        }

        synchronized (branchStats) {
            branchStats.put(branchName, stats);
        }
    }

    private int getCommitCount(String fromRef, String toRef) throws GitProcessException {
        String output = gitExecutor.git("rev-list", "--count", String.format("%s..%s", fromRef, toRef));
        return Integer.parseInt(output);
    }

    private void updateStatsFromStatus(Stats stats) throws GitProcessException {
        String status = gitExecutor.git("status", "--porcelain");
        logger.info(status);

        Stream.of(status.split("\n"))
                .map(String::trim)
                .filter(StringUtils::isNotEmpty)
                .forEach(line -> {
                    char index = line.charAt(0);
                    char workTree = line.charAt(1);

                    synchronized (stats) {
                        stats.updateStats(index);
                        stats.updateStats(workTree);
                    }
                });
    }

    private boolean isUpstreamAvailable(Upstream upstream) {
        if (upstream == null) {
            return false;
        }

        List<String> branchNames = remoteBranchNames.get(upstream.getRemoteName());
        return branchNames != null && branchNames.contains(upstream.getRemoteBranch());
    }

    public void updateBranches(boolean updateExisting) throws GitProcessException {
        if (!updateExisting) {
            return;
        }

        for (String branchName : localBranchNames) {
            updateBranch(branchName, updateExisting);
        }
    }

    private void updateBranch(String branchName, boolean updateExisting) throws GitProcessException {
        if (!updateExisting) {
            return;
        }

        Upstream upstream = branchUpstreamMap.get(branchName);

        if (upstream == null) {
            branchUpdateStatus.put(branchName, Update.SKIP_NO_UPSTREAM);
            return;
        }

        if (!determineUpstreamIsAvailable(upstream)) {
            branchUpdateStatus.put(branchName, Update.SKIP_UPSTREAM_DELETED);
            return;
        }

        gitExecutor.git("checkout", branchName);

        String fromIsh = gitExecutor.git(REV_PARSE, branchName);
        String toIsh = gitExecutor.git(REV_PARSE, upstream.getRemoteRef());

        if (StringUtils.equals(fromIsh, toIsh)) {
            branchUpdateStatus.put(branchName, Update.UP_TO_DATE);
            return;
        }

        branchUpdateIsh.put(branchName, new FromToIsh(fromIsh, toIsh));

        if (upstream.isRebase()) {
            try {
                gitExecutor.git("rebase", upstream.getRemoteRef());
                branchUpdateStatus.put(branchName, Update.REBASED);
            } catch (GitProcessException exception) {
                gitExecutor.git("rebase", "--abort");
                branchUpdateStatus.put(branchName, Update.SKIP_CONFLICTING);
            }
        } else {
            try {
                gitExecutor.git("merge", "--ff-only", upstream.getRemoteRef());
                branchUpdateStatus.put(branchName, Update.MERGED_FAST_FORWARD);
            } catch (GitProcessException exception) {
                gitExecutor.git("merge", "--abort");
                branchUpdateStatus.put(branchName, Update.SKIP_CONFLICTING);
            }
        }

        if (new File(repositoryConfig.getDirectory(), ".gitmodules").isFile()) {
            gitExecutor.git("submodule", "sync");
            gitExecutor.git("submodule", "update");
        }
    }

    private boolean determineUpstreamIsAvailable(Upstream upstream) {
        if (upstream == null) {
            return false;
        }

        List<String> branchNames = remoteBranchNames.get(upstream.getRemoteName());

        return branchNames != null && branchNames.contains(upstream.getRemoteBranch());
    }

    private List<String> parseLocalBranches(String gitOutput) {
        String[] lines = gitOutput.split("\n");
        return Arrays.stream(lines)
                .map(branch -> branch.replaceFirst("^\\*", "").trim())
                .filter(branch -> !branch.isEmpty())
                .sorted()
                .collect(Collectors.toList());
    }

    private Map<String, List<String>> parseRemoteBranches(String gitOutput) {
        String[] lines = gitOutput.split("\n");
        return Arrays.stream(lines)
                .map(String::trim)
                .filter(branch -> !(branch.isEmpty() || branch.contains(" -> ")))
                .sorted()
                .map(branch -> branch.split("/", 2))
                .collect(Collectors.toMap(
                        chunks -> chunks[0],
                        chunks -> new LinkedList<>(Collections.singletonList(chunks[1])),
                        (left, right) -> {
                            left.addAll(right);
                            return left;
                        }
                ));
    }
}
