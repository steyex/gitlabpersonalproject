package io.bit3.mgpm.worker;

import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class RepositoryCloner {
    private final Logger logger = LoggerFactory.getLogger(RepositoryCloner.class);
    private final GitExecutor gitExecutor;
    private final RepositoryConfig repositoryConfig;

    public RepositoryCloner(Config config, RepositoryConfig repositoryConfig) {
        this.gitExecutor = new GitExecutor(config, repositoryConfig);
        this.repositoryConfig = repositoryConfig;
    }

    public boolean cloneOrReconfigureRepository() throws WorkerException, GitProcessException {
        File directory = repositoryConfig.getDirectory();
        String expectedUrl = repositoryConfig.getUrl();

        if (directory.exists()) {
            if (!directory.isDirectory()) {
                throw new WorkerException(String.format("Ignoring, \"%s\" is not a directory!", directory));
            }

            if (new File(directory, ".git").isDirectory()) {
                String actualUrl = gitExecutor.git("config", "--local", "--get", "remote.origin.url");

                if (!expectedUrl.equals(actualUrl)) {
                    logger.info("[{}] update remote url", directory);
                    gitExecutor.git("remote", "set-url", "origin", expectedUrl);
                }

                return true;
            }

            File[] children = directory.listFiles();
            if (children == null) {
                logger.warn("[{}] Ignoring, the directory could not be listed", directory);
                throw new WorkerException(String.format("Ignoring, the directory \"%s\" could not be listed", directory));
            }

            if (children.length > 0) {
                logger.warn("[{}] Ignoring, the directory is not empty", directory);
                throw new WorkerException(String.format("Ignoring, the directory \"%s\" is not empty", directory));
            }
        }

        if (!directory.exists() && !directory.mkdirs()) {
            logger.error("Could not create directory");
            return false;
        }

        try {
            cloneRepository(directory, expectedUrl);
        } catch (GitProcessException e) {
            if (e.getMessage().contains("Host key verification failed")) {
                throw new WorkerException("Host key verification failed. Please make sure you have the correct access rights and the repository exists.", e);
            } else {
                throw e;
            }
        }

        return true;
    }

    private void cloneRepository(File directory, String url) throws GitProcessException {
        gitExecutor.git(directory.getParentFile(), "clone", url, directory.toString());
        gitExecutor.git("submodule", "init");
        gitExecutor.git("submodule", "update");
    }
}
