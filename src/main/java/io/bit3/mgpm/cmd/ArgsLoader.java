package io.bit3.mgpm.cmd;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ArgsLoader {
  private final OptionsFactory optionsFactory;
  private static final Logger logger = LoggerFactory.getLogger(ArgsLoader.class);


  public ArgsLoader() {
    this(new OptionsFactory());
  }

  public ArgsLoader(OptionsFactory optionsFactory) {
    this.optionsFactory = optionsFactory;
  }

  public Args load(String[] cliArguments) {
    Args args = new Args();
    Options options = optionsFactory.create();

    DefaultParser parser = new DefaultParser();
    try {
      CommandLine cmd = parser.parse(options, cliArguments);

      if (cmd.hasOption(OptionsFactory.HELP_OPT)) {
        printHelp(options);
        return null;
      }

      setConfigOption(cmd, args);
      setDoInitOption(cmd, args);
      setShowStatusOption(cmd, args);
      setDoUpdateOption(cmd, args);
      setOmitSuperfluousWarningsOption(cmd, args);
      setThreadsOption(cmd, args);
      setLoggerLevelOption(cmd, args);
      setDeleteOption(cmd, args);

      return args;
    } catch (ParseException e) {
      logger.error("Failed to parse command-line arguments: {}", e.getMessage());
      return null;
    }
  }
  private void setDeleteOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.DELETE_OPT)) {
      args.setDelete(true);
    }
  }
  private void printHelp(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("mgpm", options, true);
  }

  private void setConfigOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.CONFIG_OPT)) {
      args.setConfig(new File(cmd.getOptionValue('c')));
    }
  }

  private void setDoInitOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.INIT_OPT)) {
      args.setDoInit(true);
    }
  }

  private void setShowStatusOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.STAT_OPT)) {
      args.setShowStatus(true);
    }
  }

  private void setDoUpdateOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.UPDATE_OPT)) {
      args.setDoUpdate(true);
    }
  }

  private void setOmitSuperfluousWarningsOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.OMIT_SUPERFLUOUS_WARNINGS_OPT)) {
      args.setOmitSuperfluousWarnings(true);
    }
  }

  private void setThreadsOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.THREADS_OPT)) {
      String value = cmd.getOptionValue(OptionsFactory.THREADS_OPT);
      if (!value.matches("\\d*[1-9]\\d*")) {
        logger.error("Option --threads must be a positive number, skipping.");
      } else {
        args.setThreads(Integer.parseInt(value));
      }
    } else if (cmd.hasOption(OptionsFactory.NO_THREADS_OPT)) {
      args.setThreads(1);
    }
  }

  private void setLoggerLevelOption(CommandLine cmd, Args args) {
    if (cmd.hasOption(OptionsFactory.VERY_VERBOSE_OPT)) {
      args.setLoggerLevel(LogLevel.DEBUG);
    } else if (cmd.hasOption(OptionsFactory.VERBOSE_OPT)) {
      args.setLoggerLevel(LogLevel.INFO);
    } else if (cmd.hasOption(OptionsFactory.QUIET_OPT)) {
      args.setLoggerLevel(LogLevel.ERROR);
    } else {
      args.setLoggerLevel(LogLevel.WARN);
    }
  }
}
